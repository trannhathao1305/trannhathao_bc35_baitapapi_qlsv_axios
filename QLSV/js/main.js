const BASE_URL = "https://633ec05e0dbc3309f3bc54c2.mockapi.io";

// bật tắt loading
var batLoading = function () {
    document.getElementById("loading").style.display = "flex";
};
var tatLoading = function () {
    document.getElementById("loading").style.display = "none";
};

// lấy danh sách từ service
var fetchDssvService = function() {
    batLoading();
    axios({
        url: `${BASE_URL}/sv`,
        method: "GET",
    })
    .then(function(res) {
        renderDanhSachSinhVien(res.data);
        tatLoading();
    })
    .catch(function(err) {
        tatLoading();
    });
};
fetchDssvService();

// render danh sách sinh viên
var renderDanhSachSinhVien = function(listSv) {
    var contentHTML = "";
    listSv.forEach(function(sv){
        contentHTML += `
        <tr>
            <td>${sv.ma}</td>
            <td>${sv.ten}</td>
            <td>${sv.email}</td>
            <td>0</td>
            <td>
                <button onclick="layThongTinChiTietSv(${sv.ma})" class="btn btn-primary">Sửa</button>
                <button onclick="xoaSv(${sv.ma})" class="btn btn-danger">Xóa</button>
            </td>
        </tr>`
    });
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

// xóa sinh viên
var xoaSv = function(idSv) {
    batLoading();
    axios({
        url: `${BASE_URL}/sv/${idSv}`,
        method: "DELETE",
    })
    .then(function(res) {
        tatLoading();
        fetchDssvService();
        Swal.fire('Xóa thành công!');
    })
    .catch(function(err) {
        tatLoading();
        Swal.fire('Xóa thất bại!');
    });
};

// thêm sinh Viên
var themSv = function() {
    batLoading();
    var sv = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/sv`,
        method: "POST",
        data: sv,
    })
    .then(function(res) {
        tatLoading();
        fetchDssvService();
        Swal.fire('Thêm thành công!');
    })
    .catch(function(err) {
        Swal.fire('Thêm thất bại!');
    })
};

// sửa sinh Viên
var layThongTinChiTietSv = function(idSv) {
    var sv = layThongTinTuForm();
    axios
        .get(`${BASE_URL}/sv/${idSv}`)
        .then(function(res){
            // showThongTinLenForm(sv.ma);
            showThongTinLenForm(res.data);
        })
        .catch(function(err) {});
};

// cập nhật sinh Viên
var capNhatSv = function() {
    batLoading();
    var sv = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/sv/${sv.ma}`,
        method: "PUT",
        data: sv,
    })
    .then(function(res) {
        tatLoading();
        Swal.fire('Cập nhật thành công!')
        fetchDssvService();
    })
    .catch(function(err) {
        Swal.fire('Cập nhật thất bại!')
    });
};